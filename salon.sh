#!/bin/bash
# script to handle salon appointments
echo -e "\n~~~~~ MY SALON ~~~~~\n"

PSQL="psql --username=freecodecamp --dbname=salon --tuples-only -c"

MAIN_MENU() {
  if [[ -n $1 ]]
  then
    echo -e "\n$1\n"
  else
    echo -e "Welcome to My Salon, how can I help you?\n"
  fi
  # get the available services
  SERVICES=$($PSQL "SELECT service_id, name FROM services ORDER BY service_id")
  # list the available services
  echo "$SERVICES" | while read SERVICE_ID BAR SERVICE_NAME
  do
    echo "$SERVICE_ID) $SERVICE_NAME"
  done
  # get user input
  read SERVICE_ID_SELECTED  
  # if not valid
  if [[ ! $SERVICE_ID_SELECTED =~ ^[0-9]+$ ]]
  then
    # send to main menu
    MAIN_MENU "Please chose a valid option. What would you like today?"
  else
    # query for the service on the database
    SERVICE_NAME_SELECTED=$($PSQL "SELECT name FROM services WHERE service_id = $SERVICE_ID_SELECTED")
    # if not found
    if [[ -z $SERVICE_NAME_SELECTED ]]
    then
      # send to main menu
      MAIN_MENU "I could not find that service. What would you like today?"
    else
      # get customer phone
      echo -e "What's your phone number?"
      read CUSTOMER_PHONE
      # query for the customer name
      CUSTOMER_NAME=$($PSQL "SELECT name FROM customers WHERE phone = '$CUSTOMER_PHONE'")
      # if not found
      if [[ -z $CUSTOMER_NAME ]]
      then
        # get the customer name
        echo -e "\nI don't have a record for that phone number, what's your name?"
        read CUSTOMER_NAME
        # insert into the db
        CUSTOMER_INSERTION_RESULT=$($PSQL "INSERT INTO customers(name, phone) VALUES ('$CUSTOMER_NAME', '$CUSTOMER_PHONE')")
      fi
      # fomatting service and customer name
      SERVICE_NAME_SELECTED=$(echo $SERVICE_NAME_SELECTED | sed -E 's/^ *| *$//g')
      CUSTOMER_NAME=$(echo $CUSTOMER_NAME | sed -E 's/^ *| *$//g')

      # get customer id
      CUSTOMER_ID=$($PSQL "SELECT customer_id FROM customers WHERE phone = '$CUSTOMER_PHONE'")
      # get the time for service
      echo -e "\nWhat time would like your $SERVICE_NAME_SELECTED, $CUSTOMER_NAME?"
      read SERVICE_TIME
      # add appointment
      APPOINTMENT_INSERTION_RESULT=$($PSQL "INSERT INTO appointments(customer_id, service_id, time) VALUES ($CUSTOMER_ID, $SERVICE_ID_SELECTED, '$SERVICE_TIME')")
      # show final message
      echo -e "\nI have put you down for a $SERVICE_NAME_SELECTED at $SERVICE_TIME, $CUSTOMER_NAME."
    fi
  fi
}

MAIN_MENU